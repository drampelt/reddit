express = require "express"
rest = require "restler"
mongoose = require "mongoose"
cluster = require "cluster"
cpus = require("os").cpus()

request = mongoose.model "request", {time: Date, url: String}

if cluster.isMaster
  cluster.fork() for cpu in cpus
else
  app = express()

  app.listen(process.env.port || 47205)

  href = /href=("|')https?:\/\/(www\.)?reddit\.com\/([-A-Z0-9+&@#\/%=~_|\?;]+)?("|')/ig
  form = /action=("|')https?:\/\/(www\.)?reddit\.com\/([-A-Z0-9+&@#\/%=~_|\?;]+)("|')/ig
  formmethod = /method=("|')?POST("|')?/ig
  welcome = /<ul class="tabmenu " >/ig

  welcomeText = '<ul class="tabmenu"><li><a href="//drmc.ca" style="/*text-shadow: 0px 0px 5px rgba(255, 255, 255, 1);*/">Reddit Mirror made by Daniel</a></li>'

  reddit = 'http://www.reddit.com'

  filter = (text) =>
    text = text.replace href, 'href="/$3"'
    text = text.replace form, 'action="/$3"'
    text = text.replace formmethod, 'method="GET"'
    text = text.replace welcome, welcomeText

  app.get "*", (req, res) ->
    do (req, res) ->
      r = new request {time: Date.now(), url: req.originalUrl}
      r.save (err)->
        console.log err if err
      rest.get('http://www.reddit.com' + req.originalUrl).on 'complete', (result) ->
        if typeof result is 'object'
          res.json result
        else
          res.send filter result
